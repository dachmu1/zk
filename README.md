# Zk

# Goal
Write a tool in rust to, learn rust, and manage my zettelkastein notes written in markdown

# Desired features

- [ ] Add or remove boxes
    a box is a purpose note storage directory
- [ ] Create new notes
- [ ] Search notes by title content
- [ ] Search notes by tags
- [ ] Search notes by note content
- [ ] Manage bibliography
- [ ] Search notes by bibliography

