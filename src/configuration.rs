use serde::{Deserialize, Serialize};
use std::{collections::HashMap, path, fs, fmt::format};
use dirs;
use toml;
use toml::Value;

const CONFIG_FOLDER: &'static str = "zkr/zkr.toml";

fn get_config_home() -> std::path::PathBuf {
    dirs::config_dir().expect("No config path found")
}

pub fn get_config_path(config_folder_name: Option<&str>) -> std::path::PathBuf {
    let mut config_folder_path: std::path::PathBuf = get_config_home().join(CONFIG_FOLDER);
    if let Some(cfolder) = config_folder_name {
        config_folder_path = get_config_home().join(config_folder_name.unwrap());
    }
    config_folder_path
    
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Configuration {
    boxes: HashMap<String, String>,
}

impl Configuration {
    pub fn list_boxes(&self) -> Vec<&str> {
        let mut box_names: Vec<&str> = Vec::new();
        for box_key in self.boxes.keys() {
            box_names.push(box_key)
        }
        dbg! { self.boxes.keys() };
        box_names
    }
    
    pub fn add_box(&mut self, box_name: &str, path: &str) {
        if !self.boxes.contains_key(box_name) {
            self.boxes.insert(box_name.to_owned(), path.to_owned());
        } else {
            println!("Box {} could not be set, it already exist, and it's located at: {}", box_name, self.boxes[box_name]);
        }
    }

    pub fn update_box(&mut self, box_name: &str, path: &str) {
        if self.boxes.contains_key(box_name) {
            self.boxes.insert(box_name.to_owned(), path.to_owned());
        } else {
            println!("Box {} could not updated, box does not exist", box_name);
        }
    }
    
    pub fn remove_box(&mut self, box_name: &str) {
        if self.boxes.contains_key(box_name) {
            let tmp = self.boxes.remove(box_name);
        } else {
            println!("Box {} could not removed, box does not exist", box_name);
        }
    }
    pub fn new(toml_string: Option<&str>) -> Configuration {
        match toml_string {
            Some(config) => { 
                let boxes = toml::from_str(config).unwrap();
                Self { boxes }
            },
            None => {

                let home_dir = dirs::home_dir().unwrap();
                let home = home_dir.as_path().to_str().unwrap();
                let mut boxes: HashMap<String,String> = HashMap::new();
                boxes.insert("fleeting".to_string(), format!("{}/fleeting/", home));
                boxes.insert("fleeting".to_string(), format!("{}/slipbox/", home));
                boxes.insert("fleeting".to_string(), format!("{}/literary/", home));

                Self { boxes }
                
            }
        }
    }

}

pub fn check_config(config_path: &str) {
    match std::env::var(config_path) {
        Ok(v) => println!("{}: {}", config_path, v),
        Err(e) => panic!("${} is not set ({})", config_path, e)
    }
}

//TODO
pub fn read_configuration() -> Configuration {
    
    let mut test_data = Configuration::new(None);
    test_data
    
}

pub fn write_configuration(configuration: &Configuration, target_file: Option<std::path::PathBuf>) -> Result<(), std::io::Error> {
    let mut full_path: std::path::PathBuf = get_config_path(None);
    
    match target_file {
        Some(path) => full_path = path,
        None => {},
    }

    let file_name: Option<&std::ffi::OsStr> = AsRef::<std::path::Path>::as_ref(&full_path).file_name();
    let parent_path: Option<&std::path::Path> = AsRef::<std::path::Path>::as_ref(&full_path).parent();

    std::fs::create_dir_all(parent_path.expect("Configuration folder could not be created\n"));
    let file = std::fs::File::create(full_path.to_owned());
    dbg! { file };


    let toml_string = toml::to_string(configuration).expect("Could not encode configuration in toml");
    std::fs::write(full_path, toml_string );
    Ok(())
}

//Unit tests
//can be run with cargo test -- --nocapture for debugging
#[cfg(test)]
mod configuration_tests {
    use super::*;

    fn gen_config_sample_string() -> String {
        let test_data = r#"
                [boxes]
                fleeting = "/home/lux/sync/Athena/fleeting/"
                TODO = "$HOME/sync/Athena/TODO/"
            "#;
        test_data.to_string()
    }

    #[test]
    fn configuration_written() {

        let config_path = "./tmp/configuration_written/zkr.toml";
        let sample_config = gen_config_sample_string();
        let config: Configuration = toml::from_str(&sample_config[..]).unwrap();

        write_configuration(
            &config,
            Some(std::path::PathBuf::from(&config_path))
        );

        let config_exist = fs::metadata(config_path).is_ok();
        assert!(config_exist, "Config file {:?} does not exist", config_path);

        let cleaned = fs::remove_file(config_path).is_ok();
    }
}
