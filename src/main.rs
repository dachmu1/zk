#![allow(unused)]


mod configuration;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    let config_path = std::dbg! { configuration::get_config_path(None) };
    let mut config: configuration::Configuration = configuration::read_configuration();
    
    print!("Found args: \n");
    dbg! { args };

    dbg! {
        config.update_box("TODO", "/home/lux/sync/Athena/TODO/")
    }
    configuration::write_configuration(&config, None);
}

